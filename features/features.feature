Feature:
  Test Functionality

  Scenario: Valid scenario(changed in BitBucket)
    Given the new system
    When test functionality is implemented
    And user uses valid data
    Then user experience must be enhanced.
    
  Scenario: Invalid Scenario
    Given the new system
    When test functionality is implemented
    And user uses invalid data
    Then error message should be displayed.